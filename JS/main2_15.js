//Площадь треугольника, Задача №1
console.log('-----------------------------------------------------------------------------------------');
console.log('Задача №1 - Площадь прямоугольника')

let x1 = 6;
let y1 = 8;
let x2 = 4;
let y2 = 7;

let summ = Math.abs(x1 - x2) * Math.abs(y1 - y2);
console.log('для x1=6, y1=8, x2=4, y2=7 площадь равна', summ);
console.log('для x1=9, y1=16, x2=2, y2=85 площадь равна 483');
console.log('для x1=8, y1=5, x2=4, y2=11 площадь равна 24');
console.log('для x1=6, y1=58, x2=124, y2=79 площадь равна 2478');

console.log('-----------------------------------------------------------------------------------------');

//Работа с числами (округление, сравнение)
console.log('Задача №2 - Сравнение чисел, выделение дробной части')
let a = 0.85962;
let b = 0.233;
let precision = 3;

let Noremalizationa = Math.round(a * Math.pow(10, precision));
let Noremalizationb = Math.round(b * Math.pow(10, precision));

//Сравнение, Целая часть и остаток
console.log('Нормализованные числа', 'a=',Noremalizationa, 'b=',Noremalizationb);
console.log('Исходные числа:','a=',a,'b=', b);
console.log('числа равны', Noremalizationa === Noremalizationb);
console.log('a > b', Noremalizationa > Noremalizationb);
console.log('a < b', Noremalizationa < Noremalizationb);
console.log('a <= b', Noremalizationa <= Noremalizationb);
console.log('a >= b', Noremalizationa >= Noremalizationb);
console.log('a !== b', Noremalizationa !== Noremalizationb);

let Na = Math.floor(a);
let Nb = Math.floor(b);
let DrobA = Math.round(Math.pow(10, precision)) * (a % 1);
let DrobB = Math.round(Math.pow(10, precision)) * (b % 1);

console.log('Для a=0.85962, b=0.233, n=3 дробные части:', DrobA,',', DrobB);
console.log('Для a=0.85421654, b=0.1501203210, n=5 дробные части:  85421.65400000001 , 15012.0321');
console.log('Для a=0.25487, b=0.174547, n=2 дробные части: 25.487 , 17.454700000000003');
console.log('Для a=0.148, b=0.124, n=4 дробные части: 1480 , 1240');
console.log('Для a=a = 0.00580455, b=0.000000004560, n=7 дробные части:  58045.5 , 0.0456');

console.log('-----------------------------------------------------------------------------------------');

console.log('Задача №3 - Универсальный код (рандомные числа), сравнение результатов')

let n = -124;
let m = 586;

let pull = Math.abs(n - m);

let Npull = Math.round(Math.random() * pull);
let Npull1 = Math.round(Math.random() * pull);
let min = Math.min(n, m);
let max = Math.max(n, m);

let Nrandom = Math.round(min + Npull);
let Nrandom1 = Math.round(min + Npull1);
console.log('для n = -124, m = 586 Случайное число №1=',Nrandom, 'Случайное число №2=', Nrandom1);
console.log('Случайное число №1 = Случайное число №2', Nrandom === Nrandom1);
console.log('Случайное число №1 > Случайное число №2', Nrandom > Nrandom1);
console.log('Случайное число №1 < Случайное число №2', Nrandom < Nrandom1);
console.log('Случайное число №1 <= Случайное число №2', Nrandom <= Nrandom1);
console.log('Случайное число №1 >= Случайное число №2', Nrandom >= Nrandom1);
console.log('Случайное число №1 !== Случайное число №2', Nrandom !== Nrandom1);
console.log('наибольшее число=', max, 'минимальное число=', min);


