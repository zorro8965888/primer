//Вычисление квадратного уравнения через функцию.

function xqratC(a, b, c) {
    let d = b * b - 4 * a * c;

    if (d < 0){
        return [];
    } 
    
    else {
        if (d === 0) {
            return [-b / (2 * a)];
        }
        let droot = Math.sqrt(d);
        return [(-b + droot) / (2 * a), (-b - droot) / (2 * a)];
    }
}

xqratC(1, 2, 3);
//как эту хуету запустить??

