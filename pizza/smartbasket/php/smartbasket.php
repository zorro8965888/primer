<?php
// Включение вывода ошибок для отладки
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Проверка метода запроса
if (!isset($_SERVER['REQUEST_METHOD'])) {
    die('Request method is not set');
}

$requestMethod = $_SERVER['REQUEST_METHOD'];

// Проверка метода запроса
if ($requestMethod == 'POST') {
    // Обработка данных формы
    // ...
} else {
    // Если это не POST, возвращаем ошибку
    http_response_code(405);
    die('Method Not Allowed');
}

// Код для отправки заголовков
header('Content-Type: application/json');


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once($_SERVER['DOCUMENT_ROOT'] . '/smartbasket/php/config.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['userName'])) {
        if (empty($_POST['userName'])) {
            echo 'notName';
        } else {
            $name = "<b>Имя: </b>" . strip_tags($_POST['userName']) . "; ";
        }
    }
    if (isset($_POST['userTel'])) {
        if (empty($_POST['userTel'])) {
            echo 'notTel';
        } else {
            $tel = "<b>Телефон: </b>" . strip_tags($_POST['userTel']) . "<br>";
        }
    }
    if (isset($_POST['userEmail'])) {
        if (empty($_POST['userEmail'])) {
            echo 'notEmail';
        } else {
            $email = "<b>Email: </b>" . strip_tags($_POST['userEmail']) . "<br>";
        }
    }
    if (isset($_POST['agreement'])) {
        if (empty($_POST['agreement'])) {
            echo 'notAgreement';
        } else {
            $agreement = "<b>Соглашение: </b>" . strip_tags($_POST['agreement']) . "<br>";
        }
    }
    if (isset($_POST['finalPrice'])) {
        $finalPrice = "<b>Общая стоимость: </b>" . strip_tags($_POST['finalPrice']) . "<br>";
    }

    // Создание тела письма
    $productArr = [];
    $body = '';
    $bodyHeader = '<table border="0" cellpadding="0" cellspacing="0" style="border-bottom:1px; border-right:1px; border-color:#e2e2e2; border-style: solid; width:800px" width="800" align="center">
        <tr>
            <th colspan="3" style="width: 400px; padding:15px; text-align:center; border:1px solid #e2e2e2;">' . $name . $tel . $finalPrice . '</th>
            <th colspan="4" style="width: 400px; padding:15px; text-align:center; border:1px solid #e2e2e2;">' . $email . $agreement . '</th>
        </tr>';

    foreach ($_POST as $key => $value) {
        if (is_array($value)) {
            $body .= '<tr>';
            foreach ($value as $k => $v) {
                if ($k == 'productImg') {
                    $productImg = '<img src="' . $v . '" width="100" height="100" alt="картинка товара">';
                    $body .= '<td style="width: 100px; padding:15px; text-align:center; border:1px solid #e2e2e2;">' . $productImg . '</td>';
                }
                if ($k == 'productName') {
                    $body .= '<td style="width: 300px; padding:15px; text-align:center; border:1px solid #e2e2e2;">' . $v . '</td>';
                }
                if ($k == 'productSize') {
                    $body .= '<td style="width: 100px; padding:15px; text-align:center; border:1px solid #e2e2e2;">Размер: ' . ($v ?: 'Отсутствует') . '</td>';
                }
                if ($k == 'productId') {
                    $body .= '<td style="width: 100px; padding:15px; text-align:center; border:1px solid #e2e2e2;">ID: ' . $v . '</td>';
                }
                if ($k == 'productPrice') {
                    $body .= '<td style="width: 100px; padding:15px; text-align:center; border:1px solid #e2e2e2;">Цена: ' . $v . '</td>';
                }
                if ($k == 'productQuantity') {
                    $body .= '<td style="width: 100px; padding:15px; text-align:center; border:1px solid #e2e2e2;">Кол-во: ' . $v . '</td>';
                }
                if ($k == 'productPriceCommon') {
                    $body .= '<td style="width: 100px; padding:15px; text-align:center; border:1px solid #e2e2e2;">Общая цена: ' . $v . '</td>';
                }
            }
            $body .= '</tr>';
        }
    }
    $bodybottom = '</table>';

    // Настройка PHPMailer
    $mail = new PHPMailer(true);
    try {
        $mail->isSMTP();
        $mail->Host = HOST;
        $mail->SMTPAuth = true;
        $mail->Username = LOGIN;
        $mail->Password = PASS;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = PORT;

        $mail->setFrom(SENDER);
        $mail->addAddress(CATCHER);
        if (defined('CATCHER2')) {
            $mail->addAddress(CATCHER2);
        }

        $mail->CharSet = CHARSET;
        $mail->isHTML(true);
        $mail->Subject = SUBJECT;
        $mail->Body = "$bodyHeader $body $bodybottom";

        if (!$mail->send()) {
            echo 'Ошибка отправки сообщения.';
        } else {
            echo 'Сообщение успешно отправлено!';
        }
    } catch (Exception $e) {
        echo "Ошибка: {$mail->ErrorInfo}";
    }
} else {
    header("Location: /");
    exit;
}
